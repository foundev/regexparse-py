import pandas as pd
from typing import Callable, Dict, Sequence
from matplotlib import pyplot as plt


def create_plot(
    data: Dict[str, int],
    title: str,
    colums: Sequence[str],
    label_func: Callable[[str], str],
    index: str,
    chart_type: str,
    subplot=True,
) -> plt:
    """maps data to plot and returns it"""
    headers = None
    labels = None
    for f, drops in data.items():
        if len(drops) == 0:
            continue
        label = label_func(f)
        drops_pd = pd.DataFrame(list(drops.items()), columns=colums)
        ax = drops_pd.set_index(index).drops.plot(
            kind=chart_type, label=label, grid=True, subplots=subplot
        )
        headers, labels = ax.get_legend_handles_labels()
    plt.title(title)
    plt.xticks(rotation=60)
    if headers is not None:
        plt.legend(
            headers,
            labels,
            bbox_to_anchor=(1, 1, 1, 0),
            loc="lower right",
            mode="expand",
            ncol=2,
        )
    return plt


def create_bar(data: Dict[str, str], columns: Sequence[str]) -> pd.DataFrame:
    new_pd = pd.DataFrame(list(data.items()), index=data.keys(), columns=columns)
    new_pd.plot(kind="bar")
    return new_pd
