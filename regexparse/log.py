# Copyright 2021 Ryan SVIHLA
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import os
import re
from typing import Callable, Sequence, Dict
from datetime import datetime


def get_files_in(d: str, base_file_name: str) -> Sequence[str]:
    """looks for a base file name string inside the file name and returns all matches, default is system.log"""
    found_files = []
    for root, _, files in os.walk(d, topdown=False):
        for name in files:
            if base_file_name in name:
                found_files.append(os.path.join(root, name))
    return found_files


def count_regex_by_key(
    regex: re.Match,
    files: Sequence[str],
    key_func: Callable[[re.Match, str], str],
    count_func: Callable[[re.Match], int],
    sum_total: bool,
    start_date: datetime,
    end_date: datetime,
) -> Dict[str, int]:
    """"""
    data = {}
    for f in files:
        for line in open(f):
            drop_match = regex.search(line)
            if drop_match:
                raw_date = drop_match.group("date")
                date = datetime.strptime(raw_date, "%Y-%m-%d %H:%M:%S,%f")
                if start_date is not None and date < start_date:
                    continue
                if end_date is not None and date > end_date:
                    continue
                key = key_func(drop_match, f)
                count = count_func(drop_match)
                if key in data:
                    data[key] += count
                else:
                    data[key] = count
    return data
