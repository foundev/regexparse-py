# regexparse-py

I realized a lot of logs I parsed for my reports relied on regex, and often these regexes were pretty valuable and existed only in one place, my head. If I was lucky I had
thrown them into a gist somewhere, but I got tired of having to repeat this over and over. So now I have written a library that I can reference.

This repo is the python one.

## What version of Python

3.6 and up

## License

Apache 2.0
